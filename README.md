# BlackLotus_Ioc_scan_Powershell

Powershell script(s) to scan windows PC for published IoCs of BlackLotus bootkit documented by [Eset](https://www.welivesecurity.com/2023/03/01/blacklotus-uefi-bootkit-myth-confirmed/) and [Microsoft](https://www.microsoft.com/en-us/security/blog/2023/04/11/guidance-for-investigating-attacks-using-cve-2022-21894-the-blacklotus-campaign/)



## Usage:

Open Powershell (as Admin) and run: 

<code>.\Black-Lotus_check.ps1 </code>


![Execution](./Execution.PNG)


There also another script module from [Github](https://github.com/mattifestation/TCGLogTools) (and recommended by Mycrosoft on their blogpost about this vulnerability) that can be used to inspect Bios boot logs and changes.

To use them, import module from their repo in Powershell:

<code> Import-Module .\TCGLogTools.psm1 </code>

And then follow instructions on their git and Microsoft blogpost:

<code> .\ListBootApps.ps1 </code>

![ListBootApps](./ListBootAppsFromLogs.png)




