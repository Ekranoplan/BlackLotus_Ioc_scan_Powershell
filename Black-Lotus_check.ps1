# BlackLotus Bootkit IoC scan from Microsodft: https://www.microsoft.com/en-us/security/blog/2023/04/11/guidance-for-investigating-attacks-using-cve-2022-21894-the-blacklotus-campaign/

function Green
{
    process { Write-Host $_ -ForegroundColor Green }
}

function Red
{
    process { Write-Host $_ -ForegroundColor Red }
}


$directory = "C:\Windows\Boot\EFI"

# Check if winload.efi file exists
Write-Host "Checking if suspicious .efi files are present (True = found, this is an alert!!!, if False all good :) `n"
$dataLogExists = Test-Path -Path "$directory\winload.efi"
####dir $directory
# Check if bootmgfw.efi file exists
$errorLogExists = Test-Path -Path "$directory\bootmgfw.efi"

# Check if grubx64.efi file exists
$errorLogExists = Test-Path -Path "$directory\grubx64.efi"

# Get all .efi files in the directory
$logFiles = Get-ChildItem -Path $directory -Filter "*.log"

# Check if there are additional .log files
$additionalLogFilesExist = $logFiles.Count -gt 2

# Output the results


# Get all .log files in the directory
$logFiles = Get-ChildItem -Path $directory -Filter "*.log"

# Check if there are additional .log files
$additionalLogFilesExist = $logFiles.Count -gt 2

# Output the results with color
Write-Host "winload.efi exists: " -NoNewline
if ($dataLogExists) {
    Write-Host "True" -ForegroundColor Red
} else {
    Write-Host "False" -ForegroundColor Green
}

Write-Host "bootmgfw.efi exists: " -NoNewline
if ($errorLogExists) {
    Write-Host "True" -ForegroundColor Red
} else {
    Write-Host "False" -ForegroundColor Green
}

Write-Host "grubx64.efi exists: " -NoNewline
if ($grubExists) {
    Write-Host "True" -ForegroundColor Red
} else {
    Write-Host "False" -ForegroundColor Green
}

Write-Host "Additional .efi files exist: " -NoNewline
if ($additionalLogFilesExist) {
    Write-Host "True" -ForegroundColor Red
} else {
    Write-Host "False" -ForegroundColor Green
}

#Write-Host "winload.efi exists: $dataLogExists"
#Write-Host "bootmgfw.efi exists: $errorLogExists"
#Write-Host "grubx64.efi exists: $errorLogExists"
#Write-Host "Additional .efi files exist: $additionalLogFilesExist"

Write-Host "######################################"

Write-Host "In C:\Windows\Boot\EFI only following files should be present `n mbootmgfw.efi `n bootmgr.efi `n memtest.efi `n "
#Get-ChildItem "C:\Windows\Boot\EFI" -Filter *.efi

$directory = "C:\Windows\Boot\EFI"

# Get .efi files in the directory
$efiFiles = Get-ChildItem -Path $directory -Filter *.efi

# Process each file and output the filename with color
foreach ($file in $efiFiles) {
    if ($file.Name -match "^(bootmgfw|bootmgr|memtest)\.efi$") {
        Write-Host $file.Name -ForegroundColor Green
    } else {
        Write-Host $file.Name -ForegroundColor Red
    }
}

#Registry Check
Write-Host "######################################"
Write-Host "`nCheckng for BlackLotus registry presence in registry: `n HKLM:\\SYSTEM\CurrentControlSet\Control\DeviceGuard\Scenarios\HypervisorEnforcedCodeIntegrity `n (False is Good, True is BAD)"

function Check-RegistryKey {
    param (
        [Parameter(Mandatory = $true)]
        [string]$RegistryKeyPath
    )

    # Check if the Registry key exists
    $keyExists = Test-Path $RegistryKeyPath

    # Output the result with color
    Write-Host "Registry key exists: " -NoNewline
    if ($keyExists) {
        Write-Host $keyExists -ForegroundColor Red
    } else {
        Write-Host $keyExists -ForegroundColor Green
    }
}

# Specify the Registry key path
$registryKeyPath = "HKLM:\SYSTEM\CurrentControlSet\Control\DeviceGuard\Scenarios\HypervisorEnforcedCodeIntegrity"

# Call the function
Check-RegistryKey -RegistryKeyPath $registryKeyPath

#Event logs Check
Write-Host "######################################"
Write-Host "checking for suspicious Event IDs of 3002 and 7023 in Microsoft-Windows-Windows for failures/disable Defender/Operational (Application and Services logs > Microsoft > Windows > Windows Defender "
$logName = "Microsoft-Windows-Windows Defender/Operational"
$eventIDs = 3002, 7023

# Define the filter hashtable
$filterHashtable = @{
    LogName = $logName
    ID = $eventIDs
}

# Retrieve the event logs matching the filter
$events = Get-WinEvent -FilterHashtable $filterHashtable -ErrorAction SilentlyContinue

# Check if logs are found
if ($events) {
    # Output the filtered events in a table-like view
    $events | Format-Table -AutoSize
    Write-Host -ForegroundColor Red "True"
} else {
    # Output "False" in green color if no events are found
    Write-Host -ForegroundColor Green "False"
}

