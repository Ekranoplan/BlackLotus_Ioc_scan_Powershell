$TCGLogBytes = Get-TCGLogContent -LogType SRTMCurrent
$TCGLog = ConvertTo-TCGEventLog -LogBytes $TCGLogBytes
$PCR4 = $TCGLog.Events.PCR4
foreach ($Event in $PCR4) {
    if ($Event.EventType -eq "EV_EFI_BOOT_SERVICES_APPLICATION") {
        $DevicePath = $Event.Event.DevicePath
        if ($DevicePath -is [array]) {
            foreach ($Device in $DevicePath) {
                if (($Device.Type -eq "MEDIA_DEVICE_PATH") -and ($Device.SubType -eq "MEDIA_FILEPATH_DP")) {
                     Write-Host "Boot application:", $Device.DeviceInfo.PathName
                }
            }
        } else {
            $PathName = $DevicePath.DeviceInfo.PathName
            Write-Host "Boot application:", $PathName
        }
    }
}